package br.gov.seec;

import java.util.HashSet;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.gov.seec.models.ERole;
import br.gov.seec.models.Role;
import br.gov.seec.models.User;
import br.gov.seec.repositories.RoleRepository;
import br.gov.seec.repositories.UserRepository;

@SpringBootApplication
public class SeecApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeecApplication.class, args);
	}

	@Bean
	public CommandLineRunner setup(RoleRepository roleRepository, UserRepository userRepository){
		return (args) -> {
			Role role = new Role();
			role.setId(1);
			role.setName(ERole.ROLE_USER);
			roleRepository.save(role);
			role.setId(2);
			role.setName(ERole.ROLE_ADMIN);
			roleRepository.save(role);
			role.setId(3);
			role.setName(ERole.ROLE_MODERATOR);
			roleRepository.save(role);
			
			User user = new User();
			Set<Role> roles = new HashSet<>();
			role.setName(ERole.ROLE_USER);
			roles.add(role);
			user.setId(1L);
			user.setRoles(roles);
			user.setUsername("USUARIO_COMUN");
			user.setPassword("123456");
			user.setEmail("user@user.com");
			userRepository.save(user);

			role.setName(ERole.ROLE_MODERATOR);
			roles.add(role);
			user.setId(2L);
			user.setRoles(roles);
			user.setUsername("USUARIO_MODERADOR");
			user.setPassword("123456");
			user.setEmail("moderador@moderador.com");
			userRepository.save(user);

			role.setName(ERole.ROLE_ADMIN);
			roles.add(role);
			user.setId(3L);
			user.setRoles(roles);
			user.setUsername("USUARIO_ADMIN");
			user.setPassword("123456");
			user.setEmail("admin@admin.com");
			userRepository.save(user);
		};
	}
	

}
