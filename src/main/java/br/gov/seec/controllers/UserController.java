package br.gov.seec.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.seec.models.User;
import br.gov.seec.services.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
	UserService service;

    @GetMapping
	@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('ROLE_USER')")
	public List<User> usuarios() {
		return service.findAll();
	}

    
}
